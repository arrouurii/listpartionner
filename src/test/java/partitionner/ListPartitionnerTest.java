package partitionner;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;
public class ListPartitionnerTest {
    @Test
    public void emptyList() {
        int[] t = new int[]{};
        try {
            ListPartitionner.partition(t, 1);
        } catch (Exception e) {
            assertTrue(e.getMessage().contains("Null or empty list"));
        }
    }
    // -----------------------------------------
    @Test
    public void nullList() {
        int[] t = null;
        try {
            ListPartitionner.partition(t, 1);
        } catch (Exception e) {
            assertTrue(e.getMessage().contains("Null or empty list"));
        }
    }
    // -----------------------------------------
    @Test
    public void subsetsWithSizeEqualOrLowerThanZero() {
        int[] t = new int[]{1,2};
        try {
            ListPartitionner.partition(t, 0);
        } catch (Exception e) {
            assertTrue(e.getMessage().contains("Incorrect sublist max size"));
        }
    }
    // ---------------------------------------
    @Test
    public void subsetsWithSizeEqualToListLength() {
        int[] t = new int[]{1,2};
        int[][] result = new int[0][];
        try {
            result = ListPartitionner.partition(t, 2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertEquals("size is equal to one", result.length, 1);
        assertArrayEquals("subsetsWithSizeEqualToListLength", result[0], t);
    }
    @Test
    public void subsetsWithSizeGreaterThanListLength() {
        int[] t = new int[]{1,2};
        int[][] result = new int[0][];
        try {
            result = ListPartitionner.partition(t, 3);
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertEquals("size is equal to one", result.length, 1);
        assertArrayEquals("subsetsWithSizeEqualToListLength", result[0], t);
    }
    // -----------------------------------------------
    @Test
    public void subsetsWithEqualSize() {
        int[] t = new int[]{1,2,3,4,5,6};
        int[][] result = new int[0][];
        try {
            result = ListPartitionner.partition(t, 2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertEquals("size is equal to three", result.length, 3);
        assertArrayEquals("first sublist is equal to [1,2]", result[0], Arrays.copyOfRange(t,0, 2 ));
        assertArrayEquals("second sublist is equal to [3,4]", result[1], Arrays.copyOfRange(t,2, 4 ));
        assertArrayEquals("second sublist is equal to [5,6]", result[2], Arrays.copyOfRange(t,4, t.length ));
    }
    // -----------------------------------------------
    @Test
    public void subsetsWithDifferentSize() {
        int[] t = new int[]{1,2,3,4,5};
        int[][] result = new int[0][];
        try {
            result = ListPartitionner.partition(t, 3);
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertEquals("size is equal to two", result.length, 2);
        assertArrayEquals("first sublist is equal to [1,2,3]", result[0], Arrays.copyOfRange(t,0, 3 ));
        assertArrayEquals("second sublist is equal to [4,5]", result[1], Arrays.copyOfRange(t,3, t.length ));
    }
    // -----------------------------------------------
    @Test
    public void subsetsWithOneElementForEeach() {
        int[] t = new int[]{1,2,5,8,6};
        int[][] result = new int[0][];
        try {
            result = ListPartitionner.partition(t, 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertEquals("size is equal to three", result.length, t.length);
        assertArrayEquals("first sublist is equal to [1]", result[0], Arrays.copyOfRange(t,0, 1 ));
        assertArrayEquals("second sublist is equal to [2]", result[1], Arrays.copyOfRange(t,1, 2 ));
        assertArrayEquals("third sublist is equal to [4]", result[2], Arrays.copyOfRange(t,2, 3 ));
        assertArrayEquals("third sublist is equal to [8]", result[3], Arrays.copyOfRange(t,3, 4 ));
        assertArrayEquals("third sublist is equal to [6]", result[4], Arrays.copyOfRange(t,4, t.length ));
    }
    // -----------------------------------------------
    @Test
    public void subsetsWithPairMaxSizeFromImpairSetsSize() {
        int[] t = new int[]{1,2,5,8,6};
        int[][] result = new int[0][];
        try {
            result = ListPartitionner.partition(t, 4);
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertEquals("size is equal to two", result.length, 2);
        assertArrayEquals("first sublist is equal to [1,2,5,8]", result[0], Arrays.copyOfRange(t,0, 4 ));
        assertArrayEquals("second sublist is equal to [6]", result[1], Arrays.copyOfRange(t,4, t.length ));
    }
    // -----------------------------------------------
    @Test
    public void subsetsWithImpairMaxSizeFromImpairSetsSize() {
        int[] t = new int[]{1,2,5,8,6,9,8,7,9,5,2};
        int[][] result = new int[0][];
        try {
            result = ListPartitionner.partition(t, 7);
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertEquals("size is equal to twoo", result.length, 2);
        assertArrayEquals("first sublist is equal to [1,2,5,8,6,9,8]", result[0], Arrays.copyOfRange(t,0, 7 ));
        assertArrayEquals("second sublist is equal to [7,9,5,2]", result[1], Arrays.copyOfRange(t,7, t.length ));
    }
    // -----------------------------------------------
    @Test
    public void subsetsWithImpairMaxSizeFromPairSetsSize() {
        int[] t = new int[]{1,2,5,8};
        int[][] result = new int[0][];
        try {
            result = ListPartitionner.partition(t, 3);
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertEquals("size is equal to two", result.length, 2);
        assertArrayEquals("first sublist is equal to [1,2,5]", result[0], Arrays.copyOfRange(t,0, 3 ));
        assertArrayEquals("second sublist is equal to [8]", result[1], Arrays.copyOfRange(t,3, t.length ));
    }
    // -----------------------------------------------
    @Test
    public void subsetsWithPairMaxSizeFromPairSetsSize() {
        int[] t = new int[]{1,2,5,8,6,9};
        int[][] result = new int[0][];
        try {
            result = ListPartitionner.partition(t, 4);
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertEquals("size is equal to two", result.length, 2);
        assertArrayEquals("first sublist is equal to [1,2,5,8]", result[0], Arrays.copyOfRange(t,0, 4 ));
        assertArrayEquals("second sublist is equal to [6,9]", result[1], Arrays.copyOfRange(t,4, t.length ));
    }

}
