package partitionner;


import exceptions.PartionningException;

import java.util.Arrays;

public class ListPartitionner {

    public static int[][] partition(int[] list, int sublistsMaxSize) throws PartionningException {
        if (list == null || list.length == 0) throw new PartionningException("Null or empty list");
        if (sublistsMaxSize < 1) throw new PartionningException("Incorrect sublist max size");
        if (sublistsMaxSize >= list.length) return new int[][]{list};
        // -----------------------------
        // -----------------------------
        int nbSubsets = (list.length % sublistsMaxSize == 0) ? (list.length / sublistsMaxSize) : ((list.length / sublistsMaxSize) + 1);
        int[][] result = new int[nbSubsets][];
        // -----------------------------
        if (sublistsMaxSize == 1) {
            for (int i = 0; i < list.length; i++) {
                result[i] = new int[]{list[i]};
            }
            return result;
        }
        // -----------------------------
        int index = 0;
        int j = 0;

        while (index < nbSubsets)
        {
            sublistsMaxSize = sublistsMaxSize > list.length?  list.length : sublistsMaxSize;
            result[index] = Arrays.copyOfRange(list, j, sublistsMaxSize);
            printArray(result[index]);
            index++;
            j = sublistsMaxSize;
            sublistsMaxSize += sublistsMaxSize;
        }
        return result;
    }

    private static void printArray(int[] t) {
        for (int i = 0; i < t.length; i++) {
            System.out.print(t[i]);
            System.out.print(",");
        }
    }

    private static void print2DArray(int[][] t) {
        System.out.println('|');
        for (int[] e : t) {
            if (e != null)
                printArray(e);
        }
        System.out.println("******END*******");
    }

}
