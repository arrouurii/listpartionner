import partitionner.ListPartitionner;

public class Main {
    public static void main(String[] args) {
        int[] list = new int[]{1,2,3,5};
        int[][] newList;
        try {
            newList = ListPartitionner.partition(list, 3);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
