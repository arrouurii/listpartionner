# List Partioning

This is a java project that includes ListPartitionner.partition method wich takes
an array of integer and a subset maximum size to then return subsets of the passed array 
with a maximum size as sprecified.

**Examples:**

ListPartitionner.partition([1,2,3,4,5], 2) return: [ [1,2], [3,4], [5] ] 
ListPartitionner.partition([1,2,3,4,5], 3) return: [ [1,2,3], [4,5] ] 
ListPartitionner.partition([1,2,3,4,5], 1) return: [ [1], [2], [3], [4], [5] ] 
